import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { InjectionToken } from '@angular/core';
export const BASE_URL = new InjectionToken<string>('BASE_URL');

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent 
{
  public forecastsDaily: [any];
  public forecastsHourly: [any];
  
  public http: HttpClient;
  public timezone: string;

  
  public showDaily() {
   

    this.getDaily();
  }

  public showHourly() {
    
    
    this.getHourly();
  }

  public latitude: number;
  public longitude: number;

 public http2:HttpClient;
  
  
  constructor(http: HttpClient)
  {
    this.http2 = http;
  }

  
  

  public getDaily() {
    var x = this.latitude;
    var y = this.longitude;
    
    var url = 'https://api.darksky.net/forecast/5b7986885e7362a19517d98a32182edb/' + x + ',' + y
       this.http2.get<any>(url)
      .subscribe(result => 
      {
          this.forecastsDaily = result.daily.data;
          this.timezone = result.timezone;
         
      }, error => console.error(error));
  }

  public getHourly() {
    var x = this.latitude;
    var y = this.longitude;
    var url = 'https://api.darksky.net/forecast/5b7986885e7362a19517d98a32182edb/' + x + ',' + y
       this.http2.get<any>(url)
      .subscribe(result => 
      {  
          this.forecastsHourly = result.hourly.data;
          this.timezone = result.timezone;
      }, error => console.error(error));
  }
}



